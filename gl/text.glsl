// shader renderujuci text (ocakava stvorec [-1,-1,1,1])
#ifdef _VERTEX_

layout(location = 0) in vec3 position;
uniform vec4 os = vec4(0, 0, 1, 1);  // (offset.xy, scale.xy)
out vec2 st;

void main()
{
	st = vec2(position.x, -position.y)/2.0 + 0.5;  // flip
	gl_Position = vec4(os.xy + os.zw*position.xy, 0 , 1);
}

#endif  // _VERTEX_

#ifdef _FRAGMENT_

uniform sampler2D s;
in vec2 st;
out vec4 fcolor;

void main()
{
	fcolor = vec4(texture(s, st).rrr, 1);
}

#endif  // _FRAGMENT_
