#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <Magick++.h>
#include <ft2build.h>
#include FT_GLYPH_H
#include FT_FREETYPE_H
#include "gl/program.hpp"
#include "gl/texture.hpp"
#include "gl/mesh.hpp"
#include "gl/window.hpp"
#include "gl/shapes.hpp"
#include "gl/controllers.hpp"
#include "gl/scene_object.hpp"

using std::min;
using std::max;
using glm::mat4;
using glm::vec3;
using glm::mat3;
using glm::translate;
using glm::rotate;
using glm::scale;
using glm::inverseTranspose;
using gl::mesh;
using gl::shape_generator;
using shader::program;

char const * font_path = "/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-R.ttf";


char const * shaded_shader_source = R"(
	// zozbrazi model s tienovanim zalozenom na normale vrchola
	uniform mat4 local_to_screen;
	uniform mat3 normal_to_world;
	uniform vec3 color = vec3(0.7, 0.7, 0.7);
	uniform vec3 light_dir = normalize(vec3(1,2,3));

	#ifdef _VERTEX_
	layout(location=0) in vec3 position;
	layout(location=2) in vec3 normal;
	out vec3 n;
	void main() {
		n = normal_to_world * normal;
		gl_Position = local_to_screen * vec4(position, 1);
	}
	#endif
	#ifdef _FRAGMENT_
	in vec3 n;
	out vec4 fcolor;
	void main() {
		fcolor = vec4(max(dot(n, light_dir), 0.2) * color, 1);
	}
	#endif
)";


namespace experimental {

char const * text_shader_source = R"(
	// shader renderujuci text (v podobe textury)
	uniform mat4 local_to_screen;
	uniform sampler2D s;
	#ifdef _VERTEX_
	layout(location = 0) in vec3 position;  // stvorec (-1,-1), (1,1)
	layout(location = 1) in vec2 texcoord;
	out vec2 uv;
	void main()	{
		uv = texcoord;
		gl_Position = local_to_screen * vec4(position, 1);
	}
	#endif
	#ifdef _FRAGMENT_
	in vec2 uv;
	out vec4 fcolor;
	void main()	{
		fcolor = vec4(texture(s, uv).rrr, 1);
	}
	#endif
)";

class label
{
public:
	label();
	~label();
	void init(std::string const & file_name, unsigned size, unsigned screen_w,
		unsigned screen_h, glm::mat4 const & proj, glm::vec3 const & pos = glm::vec3{0});
	void font(std::string const & file_name, unsigned size);
	void text(std::string const & s);
	void render(glm::mat4 const & world_to_camera);
	void position(glm::vec3 const & pos);
	void projection(glm::mat4 const & proj);
	void screen_geometry(unsigned w, unsigned h);
	unsigned width() const;
	unsigned height() const;

private:
	void build_text_texture();
	void build_ltw_transform();
	std::vector<FT_Glyph> load_glyphs(std::string const & s);

	std::string _text;
	glm::vec3 _pos;
	unsigned _screen_w, _screen_h;
	glm::mat4 _proj;  //!< projection transformation
	glm::mat4 _local_to_world;
	texture2d _text_tex;
	shader::program _text_prog;
	FT_Library _lib;
	FT_Face _face;
	std::map<unsigned, FT_Glyph> _cache;
	unsigned const dpi = 96;
};

namespace detail {

void copy(FT_BitmapGlyph glyph, FT_Vector const & pen, Magick::Image & result);
void unite(FT_BBox & result, FT_BBox const & bbox);
void adjust(FT_BBox & result, FT_Pos dx);
unsigned width(FT_BBox const & b);
unsigned height(FT_BBox const & b);

FT_BBox measure_glyphs(std::vector<FT_Glyph> const & glyphs)
{
	if (glyphs.empty())
		return FT_BBox{0, 0, 0, 0};

	FT_BBox result;
	FT_Glyph_Get_CBox(glyphs[0], FT_GLYPH_BBOX_PIXELS, &result);

	FT_Pos advance = glyphs[0]->advance.x >> 16;  // 16.16

	for (int i = 1; i < glyphs.size(); ++i)
	{
		FT_BBox bbox;
		FT_Glyph_Get_CBox(glyphs[i], FT_GLYPH_BBOX_PIXELS, &bbox);
		adjust(bbox, advance);

		unite(result, bbox);

		advance += glyphs[i]->advance.x >> 16;  // 16.6
	}

	return result;
}

Magick::Image render_glyphs(std::vector<FT_Glyph> const & glyphs)
{
	using namespace Magick;

	FT_BBox glyphs_bbox = measure_glyphs(glyphs);
	Image result{Geometry(width(glyphs_bbox) + glyphs_bbox.xMin, height(glyphs_bbox)), "black"};  // TODO: nech je pozadie transparentne
	result.depth(8);

	FT_Vector pen{0, glyphs_bbox.yMax};

	for (FT_Glyph glyph : glyphs)
	{
		FT_BitmapGlyph glyph_bitmap = (FT_BitmapGlyph)glyph;

		FT_Error err = FT_Glyph_To_Bitmap((FT_Glyph *)&glyph_bitmap, FT_RENDER_MODE_NORMAL, nullptr, 0);
		assert(!err && "unable convert a glyph object to a glyph bitmap");

		copy(glyph_bitmap, pen, result);

		pen.x += glyph_bitmap->root.advance.x >> 16;  // 16.16
	}

	result.flip();  // TODO: toto nech sa realizuje uz pri kopirovani (v ortho transformacii netreba texturu flipnut)

	return result;
}

FT_Glyph load_glyph(unsigned char_code, FT_Face face)
{
	FT_UInt glyph_index = FT_Get_Char_Index(face, char_code);
	FT_Error err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);  // now we can access face->glyph
	assert(!err && "can't load a glyph");

	FT_Glyph glyph = nullptr;
	err = FT_Get_Glyph(face->glyph, &glyph);
	assert(!err && "unable to extract a glyph object");

	return glyph;
}

void copy(FT_BitmapGlyph glyph, FT_Vector const & pen, Magick::Image & result)
{
	using namespace Magick;

	FT_Bitmap & bitmap = glyph->bitmap;
	unsigned char * bmbuf = bitmap.buffer;

	if (bmbuf)  // some glyphs has no image data (spaces)
	{
		PixelPacket * pixels = result.getPixels(pen.x + glyph->left, pen.y - glyph->top, bitmap.width, bitmap.rows);
		assert(pixels && "out of image geometry");
		for (int i = 0; i < (bitmap.width*bitmap.rows); ++i, ++pixels, ++bmbuf)
			*pixels = ColorGray{*bmbuf/255.0};
		result.syncPixels();
	}
}

void unite(FT_BBox & result, FT_BBox const & bbox)
{
	result.xMin = min(result.xMin, bbox.xMin);
	result.xMax = max(result.xMax, bbox.xMax);
	result.yMin = min(result.yMin, bbox.yMin);
	result.yMax = max(result.yMax, bbox.yMax);
}

void adjust(FT_BBox & result, FT_Pos dx)
{
	result.xMin += dx;
	result.xMax += dx;
}

unsigned width(FT_BBox const & b)
{
	return b.xMax - b.xMin;
}

unsigned height(FT_BBox const & b)
{
	return b.yMax - b.yMin;
}

}  // detail


label::label() : _lib{nullptr}, _face{nullptr}
{}

label::~label()
{
	for (auto e : _cache)
		FT_Done_Glyph(e.second);

	FT_Done_Face(_face);
	FT_Done_FreeType(_lib);
}

void label::init(std::string const & file_name, unsigned size, unsigned screen_w,
	unsigned screen_h, glm::mat4 const & proj, glm::vec3 const & pos)
{
	assert(!_lib && !_face && "already initialized");
	_pos = pos;
	_screen_w = screen_w;
	_screen_h = screen_h;
	_proj = proj;
	_text_prog.from_memory(text_shader_source);  // TODO: toto je super neefektivne
	FT_Error err = FT_Init_FreeType(&_lib);
	assert(!err && "unable to init a free-type library");
	font(file_name, size);
}

void label::font(std::string const & file_name, unsigned size)
{
	FT_Error err = FT_New_Face(_lib, file_name.c_str(), 0, &_face);
	assert(!err && "unable to load a font face");

	unsigned font_size = size << 6;  // 26.6
	err = FT_Set_Char_Size(_face, font_size, font_size, dpi, dpi);
	assert(!err && "freetype set font size failed");

	if (!_text.empty())
		build_text_texture();
}

void label::text(std::string const & s)
{
	assert(_face && "set font first");

	if (_text == s)
		return;

	_text = s;
	build_text_texture();
}

void label::render(glm::mat4 const & world_to_camera)
{
	if (_text.empty())
		return;

	mat4 local_to_screen = _proj * world_to_camera * _local_to_world;
//	mat4 local_to_screen = world_to_camera;

	_text_prog.use();
	_text_prog.uniform_variable("s", 0);
	_text_tex.bind(0);
	_text_prog.uniform_variable("local_to_screen", local_to_screen);

	static gl::mesh m = gl::make_quad_xy<gl::mesh>();
	m.render();
}

void label::position(glm::vec3 const & pos)
{
	_pos = pos;
	build_ltw_transform();
}

void label::projection(glm::mat4 const & proj)
{
	_proj = proj;
}

void label::screen_geometry(unsigned w, unsigned h)
{
	_screen_w = w;
	_screen_h = h;
	build_ltw_transform();
}

unsigned label::width() const
{
	return _text_tex.width();
}

unsigned label::height() const
{
	return _text_tex.height();
}

void label::build_text_texture()
{
	assert(!_text.empty() && "nothing to build from");

	std::vector<FT_Glyph> glyphs = load_glyphs(_text);
	Magick::Image im = detail::render_glyphs(glyphs);
	Magick::Blob blob;
	im.write(&blob, "GRAY");

	_text_tex = texture2d(im.columns(), im.rows(), sized_internal_format::r8,
		pixel_format::red, pixel_type::ub8, blob.data());

	build_ltw_transform();
}

void label::build_ltw_transform()
{
	_local_to_world = translate(_pos) * scale(vec3{width()/(float)_screen_w, height()/(float)_screen_h, 1});
}

std::vector<FT_Glyph> label::load_glyphs(std::string const & s)
{
	std::vector<FT_Glyph> result;
	result.reserve(s.size());

	for (auto char_code : s)
	{
		auto glyph_it = _cache.find(char_code);
		if (glyph_it != _cache.end())
			result.push_back(glyph_it->second);
		else
		{
			FT_Glyph glyph = detail::load_glyph(char_code, _face);
			_cache[char_code] = glyph;
			result.push_back(glyph);
		}
	}

	return result;
}

}  // experimental


class scene_window : public ui::glut_pool_window
{
public:
	using base = ui::glut_pool_window;
	scene_window();
	void update(float dt) override;
	void display() override;
	void input(float dt) override;

private:
	mesh _cube, _box, _sphere;
	mesh _quad;
	experimental::label _lbl;
	gl::free_camera<scene_window> _view;
	axis_object _axis;
	shader::program _shaded;
};

scene_window::scene_window()
	: _view{glm::radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
{
	shape_generator<mesh> shape;
	_cube = shape.cube();
	_box = shape.box(vec3{.5, 1, .5});
	_sphere = shape.sphere(.5);
	_quad = shape.quad_xy();
	_lbl.init(font_path, 24, width(), height(), _view.get_camera().projection());
	_view.get_camera().position = vec3{3,3,3};
	_view.get_camera().look_at(vec3{0,0,0});
	_shaded.from_memory(shaded_shader_source);
	glClearColor(0,0,0,1);
}

void scene_window::update(float dt)
{
	base::update(dt);
}

void scene_window::display()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	mat4 world_to_camera = _view.get_camera().world_to_camera();
	mat4 world_to_screen = _view.get_camera().projection() * world_to_camera;

	// cube
	mat4 M{1};
	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen * M);
	_shaded.uniform_variable("normal_to_world", mat3{inverseTranspose(M)});
	_shaded.uniform_variable("color", rgb::lime);
	_cube.render();

	_lbl.text("cube");
	_lbl.position(vec3{0,1.2,0});
	_lbl.render(world_to_camera);

	// box
	M = translate(vec3{2, 0, -2});
	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen * M);
	_shaded.uniform_variable("normal_to_world", mat3{inverseTranspose(M)});
	_shaded.uniform_variable("color", rgb::purple);
	_box.render();

	_lbl.text("box");
	_lbl.position(vec3{2, 1.5, -2});
	_lbl.render(world_to_camera);

	// sphere
	M = translate(vec3{2, 0, 1});
	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen * M);
	_shaded.uniform_variable("normal_to_world", mat3{inverseTranspose(M)});
	_shaded.uniform_variable("color", rgb::cyan);
	_sphere.render();

	_lbl.text("sphere");
	_lbl.position(vec3{2, 1, 1});
	_lbl.render(world_to_camera);

	_axis.render(world_to_screen);
	base::display();
}

void scene_window::input(float dt)
{
	_view.input(dt);
	base::input(dt);
}


int main(int argc, char * argv[])
{
	scene_window w;
	w.start();
	return 0;
}
