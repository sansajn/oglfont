#include "label.hpp"
#include <algorithm>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <Magick++.h>
#include "gl/mesh.hpp"
#include "gl/shapes.hpp"

namespace ui {

using glm::mat4;
using glm::vec3;
using glm::vec2;
using glm::ortho;
using glm::translate;
using glm::scale;

	namespace detail {

using std::min;
using std::max;

void copy(FT_BitmapGlyph glyph, FT_Vector const & pen, Magick::Image & result);
void unite(FT_BBox & result, FT_BBox const & bbox);
void adjust(FT_BBox & result, FT_Pos dx);
unsigned width(FT_BBox const & b);
unsigned height(FT_BBox const & b);

FT_BBox measure_glyphs(std::vector<FT_Glyph> const & glyphs)
{
	if (glyphs.empty())
		return FT_BBox{0, 0, 0, 0};

	FT_BBox result;
	FT_Glyph_Get_CBox(glyphs[0], FT_GLYPH_BBOX_PIXELS, &result);

	FT_Pos advance = glyphs[0]->advance.x >> 16;  // 16.16

	for (int i = 1; i < glyphs.size(); ++i)
	{
		FT_BBox bbox;
		FT_Glyph_Get_CBox(glyphs[i], FT_GLYPH_BBOX_PIXELS, &bbox);
		adjust(bbox, advance);

		unite(result, bbox);

		advance += glyphs[i]->advance.x >> 16;  // 16.6
	}

	return result;
}

Magick::Image render_glyphs(std::vector<FT_Glyph> const & glyphs)
{
	using namespace Magick;

	FT_BBox glyphs_bbox = measure_glyphs(glyphs);
	Image result{Geometry(width(glyphs_bbox) + glyphs_bbox.xMin, height(glyphs_bbox)), "black"};  // TODO: nech je pozadie transparentne
	result.depth(8);

	FT_Vector pen{0, glyphs_bbox.yMax};

	for (FT_Glyph glyph : glyphs)
	{
		FT_BitmapGlyph glyph_bitmap = (FT_BitmapGlyph)glyph;

		FT_Error err = FT_Glyph_To_Bitmap((FT_Glyph *)&glyph_bitmap, FT_RENDER_MODE_NORMAL, nullptr, 0);
		assert(!err && "unable convert a glyph object to a glyph bitmap");

		copy(glyph_bitmap, pen, result);

		pen.x += glyph_bitmap->root.advance.x >> 16;  // 16.16
	}

	return result;
}

FT_Glyph load_glyph(unsigned char_code, FT_Face face)
{
	FT_UInt glyph_index = FT_Get_Char_Index(face, char_code);
	FT_Error err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);  // now we can access face->glyph
	assert(!err && "can't load a glyph");

	FT_Glyph glyph = nullptr;
	err = FT_Get_Glyph(face->glyph, &glyph);
	assert(!err && "unable to extract a glyph object");

	return glyph;
}

void copy(FT_BitmapGlyph glyph, FT_Vector const & pen, Magick::Image & result)
{
	using namespace Magick;

	FT_Bitmap & bitmap = glyph->bitmap;
	unsigned char * bmbuf = bitmap.buffer;

	if (bmbuf)  // some glyphs has no image data (spaces)
	{
		PixelPacket * pixels = result.getPixels(pen.x + glyph->left, pen.y - glyph->top, bitmap.width, bitmap.rows);
		assert(pixels && "out of image geometry");
		for (int i = 0; i < (bitmap.width*bitmap.rows); ++i, ++pixels, ++bmbuf)
			*pixels = ColorGray{*bmbuf/255.0};
		result.syncPixels();
	}
}

void unite(FT_BBox & result, FT_BBox const & bbox)
{
	result.xMin = min(result.xMin, bbox.xMin);
	result.xMax = max(result.xMax, bbox.xMax);
	result.yMin = min(result.yMin, bbox.yMin);
	result.yMax = max(result.yMax, bbox.yMax);
}

void adjust(FT_BBox & result, FT_Pos dx)
{
	result.xMin += dx;
	result.xMax += dx;
}

unsigned width(FT_BBox const & b)
{
	return b.xMax - b.xMin;
}

unsigned height(FT_BBox const & b)
{
	return b.yMax - b.yMin;
}

char const * text_shader_source = R"(
	// shader renderujuci text (v podobe textury)
	uniform mat4 local_to_screen;
	uniform sampler2D s;
	#ifdef _VERTEX_
	layout(location = 0) in vec3 position;  // stvorec (-1,-1), (1,1)
	layout(location = 1) in vec2 texcoord;
	out vec2 uv;
	void main()	{
		uv = texcoord;
		gl_Position = local_to_screen * vec4(position, 1);
	}
	#endif
	#ifdef _FRAGMENT_
	in vec2 uv;
	out vec4 fcolor;
	void main()	{
		fcolor = vec4(texture(s, uv).rrr, 1);
	}
	#endif
)";

	}  // detail


ortho_label::ortho_label() : _lib{nullptr}, _face{nullptr}
{}

ortho_label::~ortho_label()
{
	for (auto e : _cache)
		FT_Done_Glyph(e.second);

	FT_Done_Face(_face);
	FT_Done_FreeType(_lib);
}

void ortho_label::init(std::string const & file_name, unsigned size, unsigned screen_w, unsigned screen_h, glm::vec2 const & pos)
{
	assert(!_lib && !_face && "already initialized");
	_screen_w = screen_w;
	_screen_h = screen_h;
	_pos = pos;
	_text_prog.from_memory(detail::text_shader_source);  // TODO: toto je super neefektivne (zdielaj pomedzi vsetky ortho_label)
	FT_Error err = FT_Init_FreeType(&_lib);
	assert(!err && "unable to init a free-type library");
	font(file_name, size);
}

void ortho_label::font(std::string const & file_name, unsigned size)
{
	FT_Error err = FT_New_Face(_lib, file_name.c_str(), 0, &_face);
	assert(!err && "unable to load a font face");

	unsigned font_size = size << 6;  // 26.6
	err = FT_Set_Char_Size(_face, font_size, font_size, dpi, dpi);
	assert(!err && "freetype set font size failed");

	if (!_text.empty())
		build_text_texture();
}

void ortho_label::text(std::string const & s)
{
	assert(_face && "set font first");

	if (_text == s)
		return;

	_text = s;
	build_text_texture();
}

void ortho_label::render()
{
	if (_text.empty())
		return;

	_text_prog.use();
	_text_prog.uniform_variable("s", 0);
	_text_tex.bind(0);
	_text_prog.uniform_variable("local_to_screen", _local_to_screen);

	static gl::mesh m = gl::make_quad_xy<gl::mesh>();  // TODO: zdielaj pomedzi vsetky ortho_label
	m.render();
}

void ortho_label::position(glm::vec2 const & pos)
{
	_pos = pos;
	build_transform_matrix();
}

void ortho_label::screen_geometry(unsigned w, unsigned h)
{
	_screen_w = w;
	_screen_h = h;
}

unsigned ortho_label::width() const
{
	return _text_tex.width();
}

unsigned ortho_label::height() const
{
	return _text_tex.height();
}

void ortho_label::build_text_texture()
{
	assert(!_text.empty() && "nothing to build from");

	std::vector<FT_Glyph> glyphs = load_glyphs(_text);
	Magick::Image im = detail::render_glyphs(glyphs);
	Magick::Blob blob;
	im.write(&blob, "GRAY");

	_text_tex = texture2d(im.columns(), im.rows(), sized_internal_format::r8,
		pixel_format::red, pixel_type::ub8, blob.data());

	build_transform_matrix();
}

void ortho_label::build_transform_matrix()
{
	mat4 O = ortho(0.0f, (float)_screen_w, (float)_screen_h, 0.0f, 0.0f, 100.0f);
	_local_to_screen = O * translate(vec3{_pos + vec2{width()/2.0f, height()/2.0f}, 0}) * scale(vec3{width()/2.0f, height()/2.0f, 0});
}

std::vector<FT_Glyph> ortho_label::load_glyphs(std::string const & s)
{
	std::vector<FT_Glyph> result;
	result.reserve(s.size());

	for (auto char_code : s)
	{
		auto glyph_it = _cache.find(char_code);
		if (glyph_it != _cache.end())
			result.push_back(glyph_it->second);
		else
		{
			FT_Glyph glyph = detail::load_glyph(char_code, _face);
			_cache[char_code] = glyph;
			result.push_back(glyph);
		}
	}

	return result;
}

}  // ui
