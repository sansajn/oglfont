#include <Magick++.h>
#include "gl/window.hpp"
#include "gl/texture.hpp"
#include "gl/program.hpp"
#include "gl/mesh.hpp"
#include "gl/shapes.hpp"

using gl::mesh;
using gl::make_quad_xy;
using base_window = ui::window<ui::glut_event_impl>;


class main_window : public base_window
{
public:
	void display() override;
};

void main_window::display()
{
	static shader::program prog{"text.glsl"};
	prog.use();
	prog.uniform_variable("s", 0);

//	static texture2d tex{"checker3x3.png", texture::parameters().mag(texture_filter::nearest)};

	using namespace Magick;
	Image im{"checker3x3.png"};
	Blob blob;
	im.write(&blob, "RGBA");
	uint8_t const * __data = (uint8_t const *)blob.data();

	uint8_t const pixels[9*4] = {
				0,0,0,255, 255,255,255,255,       0,0,0,255,
		255,255,255,255,       0,0,0,255, 255,255,255,255,
				0,0,0,255, 255,255,255,255,       0,0,0,255};

	static texture2d tex{3, 3, sized_internal_format::rgba8, pixel_format::rgba,
		pixel_type::ub8, pixels, texture::parameters().mag(texture_filter::nearest)};

	tex.bind(0);

	static mesh quad = make_quad_xy<mesh>();
	quad.render();

	base_window::display();
}

int main(int argc, char * argv[])
{
	main_window w;
	w.start();
	return 0;
}
