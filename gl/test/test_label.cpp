// test label kontrolky
#include "window.hpp"
#include "label.hpp"

using base_window = ui::glut_event_window;

class main_window : public base_window
{
public:
	main_window();
	void display() override;

private:
	ui::label<main_window> _label;
};

main_window::main_window()
	: base_window(parameters().name("test for label component")), _label(0, 0, *this)
{
	_label.font("/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-B.ttf", 24);
	_label.text("Teresa Lisbon");
}

void main_window::display()
{
	_label.render();
	base_window::display();
}


int main(int argc, char * argv[])
{
	main_window w;
	w.start();
	return 0;
}
