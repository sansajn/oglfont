#include <string>
#include <iostream>
#include <cassert>
#include <ft2build.h>
#include FT_GLYPH_H
#include FT_FREETYPE_H

using std::cout;
using std::string;

FT_BitmapGlyph render_glyph(unsigned char_code, FT_Face face);
string pixel_mode(FT_Pixel_Mode m);


int main(int argc, char * argv[])
{
	FT_Library ftlib;
	FT_Error err = FT_Init_FreeType(&ftlib);
	assert(!err && "unable to init a free-type library");

	FT_Face face;
	err = FT_New_Face(ftlib,
		"/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-B.ttf", 0, &face);
	assert(!err && "unable to load font face");

	FT_BitmapGlyph glyph_bitmap = render_glyph('T', face);

	FT_Bitmap & bitmap = glyph_bitmap->bitmap;
	cout
		<< "w:" << bitmap.width
		<< ", h:" << bitmap.rows
		<< ", pitch:" << bitmap.pitch
		<< ", num_grays:" << bitmap.num_grays
		<< ", pixel_mode:" << pixel_mode((FT_Pixel_Mode)bitmap.pixel_mode)
		<< ", size:" << (bitmap.width*bitmap.rows)
		<< "\n";

	uint8_t * __pixels = (uint8_t *)bitmap.buffer;

	FT_Done_Glyph((FT_Glyph)glyph_bitmap);

	FT_Done_Face(face);
	FT_Done_FreeType(ftlib);

	return 0;
}

string pixel_mode(FT_Pixel_Mode m)
{
	switch (m)
	{
		case FT_PIXEL_MODE_NONE: return "none";
		case FT_PIXEL_MODE_MONO: return "mono";
		case FT_PIXEL_MODE_GRAY: return "gray";
		case FT_PIXEL_MODE_GRAY2: return "gray2";
		case FT_PIXEL_MODE_GRAY4: return "gray4";
		case FT_PIXEL_MODE_LCD: return "lcd";
		case FT_PIXEL_MODE_LCD_V: return "lcd_v";
		case FT_PIXEL_MODE_BGRA: return "bgra";
		case FT_PIXEL_MODE_MAX: return "max";
		default: assert(0);
	}
}

FT_BitmapGlyph render_glyph(unsigned char_code, FT_Face face)
{
	unsigned const dpi = 92;
	unsigned font_size = 16 << 6;  // 26.6
	FT_Error err = FT_Set_Char_Size(face, font_size, font_size, dpi, dpi);
	assert(!err && "freetype set size failed");

	FT_UInt glyph_index = FT_Get_Char_Index(face, char_code);
	err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);  // now we can access face->glyph
	assert(!err && "can't load a glyph");

	FT_Glyph glyph = nullptr;
	err = FT_Get_Glyph(face->glyph, &glyph);
	assert(!err && "unable to extract a glyph object");

	err = FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, nullptr, 1);
	assert(!err && "unable convert a glyph object to a glyph bitmap");

	return (FT_BitmapGlyph)glyph;
}
