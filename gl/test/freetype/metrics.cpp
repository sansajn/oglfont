#include <iostream>
#include <cassert>
#include <Magick++.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

void print_format(FT_Glyph_Format fmt);

int main(int argc, char * argv[])
{
	FT_Library ftlib;
	FT_Error err = FT_Init_FreeType(&ftlib);
	assert(!err && "freetype initialization failed");

	FT_Face face;
	err = FT_New_Face(ftlib,
		"/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-B.ttf", 0, &face);
	assert(!err && "unable to load font face");

	// [set proper font size]
	int const font_size = 16 << 6;  // 26.6
	err = FT_Set_Char_Size(face, font_size, font_size, 300, 300);
	assert(!err && "freetype set size failed");

	// [load glyph]
	unsigned char_code = 'e';
	FT_UInt glyph_index = FT_Get_Char_Index(face, char_code);
	err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);  // now we can access face->glyph
	assert(!err && "can't load a glyph");

	// [store glyph image]
	FT_Glyph glyph = nullptr;
	FT_GlyphSlot slot = face->glyph;
	err = FT_Get_Glyph(slot, &glyph);
	assert(!err && "unable to extract a glyph image");

	// [print glyph matrics]
	FT_Glyph_Metrics & metrics = slot->metrics;  // everithing in 26.6 format

	std::cout << "glyph metrics (from slot):\n"
		<< "  width:" << (metrics.width >> 6) << "\n"
		<< "  height:" << (metrics.height >> 6) << "\n"
		<< "  horiBearingX: " << (metrics.horiBearingX >> 6) << " (left)\n"
		<< "  horiBearingY: " << (metrics.horiBearingY >> 6) << " (top)\n"
		<< "  horiAdvance: " << (metrics.horiAdvance >> 6) << " (glyph.advance.x)\n"
		<< "  vertBearingX: " << (metrics.vertBearingX >> 6) << "\n"
		<< "  vertBearingY: " << (metrics.vertBearingY >> 6) << "\n"
		<< "  vertAdvance: " << (metrics.vertAdvance >> 6) << "\n";

	std::cout << "glyph-advance: ["
		<< (glyph->advance.x >> 16) << ", " << (glyph->advance.y >> 16) << "]\n";

	// [convert to bitmap]
	FT_BitmapGlyph glyph_bitmap = (FT_BitmapGlyph)glyph;
	err = FT_Glyph_To_Bitmap((FT_Glyph *)&glyph_bitmap, FT_RENDER_MODE_NORMAL, 0, 0);
	assert(!err && "unable to convert glyph object to a bitmap");

	std::cout << "glyph-format:";
	print_format(glyph->format);
	std::cout << "\n";

	std::cout << "bitmap-format:";
	print_format(glyph_bitmap->root.format);
	std::cout << " (after conversion)\n";

	std::cout << "bitmap glyph:\n"
		<< "  left:" << glyph_bitmap->left << "\n"
		<< "  top:" << glyph_bitmap->top << "\n";

	FT_Bitmap & bitmap = glyph_bitmap->bitmap;
	std::cout
		<< "  width:" << bitmap.width << "\n"
		<< "  rows:" << bitmap.rows << "\n";

	// control box
	FT_BBox glyph_bbox;
	FT_Glyph_Get_CBox(glyph, FT_GLYPH_BBOX_PIXELS, &glyph_bbox);
	unsigned glyph_w = glyph_bbox.xMax - glyph_bbox.xMin;
	unsigned glyph_h = glyph_bbox.yMax - glyph_bbox.yMin;
	std::cout << "glyph-control-box " << "w:" << glyph_w << ", h:" << glyph_h << "\n";

	// [render glyph to file]
	using namespace Magick;

	unsigned const w = glyph_w + glyph_bitmap->left;
	unsigned const h = glyph_h;
	unsigned const pen_y = glyph_bitmap->top;
	Image im{Geometry{w, h}, "black"};
	std::cout << "image " << "w:" << w << ", h:" << h << "\n";

	unsigned char * bmbuf = bitmap.buffer;
	PixelPacket * pixels = im.getPixels(glyph_bitmap->left, pen_y - glyph_bitmap->top, glyph_w, glyph_h);
	for (int i = 0; i < glyph_w*glyph_h; ++i, ++pixels, ++bmbuf)
		*pixels = ColorGray(*bmbuf/255.0);
	im.syncPixels();

	im.write("out/metrics.png");

	FT_Done_Glyph((FT_Glyph)glyph_bitmap);

	FT_Done_Glyph(glyph);

	return 0;
}

void print_format(FT_Glyph_Format fmt)
{
	switch (fmt)
	{
		case ft_glyph_format_none: std::cout << "none"; break;
		case ft_glyph_format_composite: std::cout << "composite"; break;
		case ft_glyph_format_bitmap: std::cout << "bitmap"; break;
		case ft_glyph_format_outline: std::cout << "outline"; break;
		case ft_glyph_format_plotter: std::cout << "plotter"; break;
		default: std::cout << "<unknown>";
	}
}
