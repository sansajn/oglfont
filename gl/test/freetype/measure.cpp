// vyrenderuje text do najmensiej oblasti
#include <algorithm>
#include <string>
#include <vector>
#include <cassert>
#include <iostream>
#include <Magick++.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

using std::min;
using std::max;
using std::vector;
using std::string;

string const word = "Teresa Lisbon";

// glyphs helpers
vector<FT_Glyph> load_glyphs(string const & s, FT_Face face);
FT_BBox measure_glyphs(vector<FT_Glyph> const & glyphs);
void render_glyphs(vector<FT_Glyph> const & glyphs, string const & file_name);

// bbox helpers
unsigned width(FT_BBox const & b);
unsigned height(FT_BBox const & b);
void adjust(FT_BBox & result, FT_Pos dx);
void unite(FT_BBox & result, FT_BBox const & bbox);

// image helpers
void copy(FT_BitmapGlyph glyph, FT_Vector const & pen, Magick::Image & result);


int main(int argc, char * argv[])
{
	FT_Library ftlib;
	FT_Error err = FT_Init_FreeType(&ftlib);
	assert(!err && "unable to init a free-type library");

	FT_Face face;
	err = FT_New_Face(ftlib,
		"/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-B.ttf", 0, &face);
	assert(!err && "unable to load font face");

	// [set proper font size]
	int const font_size = 16 << 6;  // 26.6
	err = FT_Set_Char_Size(face, font_size, font_size, 300, 300);
	assert(!err && "freetype set size failed");

	vector<FT_Glyph> glyphs = load_glyphs(word, face);
	render_glyphs(glyphs, "out/measure.png");

	return 0;
}

void render_glyphs(vector<FT_Glyph> const & glyphs, string const & file_name)
{
	using namespace Magick;

	FT_BBox glyphs_bbox = measure_glyphs(glyphs);
	Image im{Geometry{width(glyphs_bbox) + glyphs_bbox.xMin, height(glyphs_bbox)}, "black"};

	FT_Vector pen{0, glyphs_bbox.yMax};

	for (FT_Glyph glyph : glyphs)
	{
		FT_BitmapGlyph glyph_bitmap = (FT_BitmapGlyph)glyph;

		FT_Error err = FT_Glyph_To_Bitmap((FT_Glyph *)&glyph_bitmap, FT_RENDER_MODE_NORMAL, nullptr, 0);
		assert(!err && "unable convert a glyph object to a glyph bitmap");

		copy(glyph_bitmap, pen, im);

		pen.x += glyph_bitmap->root.advance.x >> 16;  // 16.16
	}

	im.write(file_name);
}

void copy(FT_BitmapGlyph glyph, FT_Vector const & pen, Magick::Image & result)
{
	using namespace Magick;

	FT_Bitmap & bitmap = glyph->bitmap;
	unsigned char * bmbuf = bitmap.buffer;

	if (bmbuf)  // some glyphs has no image data (spaces)
	{
		PixelPacket * pixels = result.getPixels(pen.x + glyph->left, pen.y - glyph->top, bitmap.width, bitmap.rows);
		assert(pixels && "out of image geometry");
		for (int i = 0; i < (bitmap.width*bitmap.rows); ++i, ++pixels, ++bmbuf)
			*pixels = ColorGray{*bmbuf/255.0};
		result.syncPixels();
	}
}

FT_BBox measure_glyphs(vector<FT_Glyph> const & glyphs)
{
	FT_BBox result;
	FT_Glyph_Get_CBox(glyphs[0], FT_GLYPH_BBOX_PIXELS, &result);

	FT_Pos advance = glyphs[0]->advance.x >> 16;  // 16.16

	for (int i = 1; i < glyphs.size(); ++i)
	{
		FT_BBox bbox;
		FT_Glyph_Get_CBox(glyphs[i], FT_GLYPH_BBOX_PIXELS, &bbox);
		adjust(bbox, advance);

		unite(result, bbox);

		advance += glyphs[i]->advance.x >> 16;  // 16.6
	}

	return result;
}

vector<FT_Glyph> load_glyphs(string const & s, FT_Face face)
{
	vector<FT_Glyph> result;
	result.reserve(s.size());

	for (auto char_code : s)
	{
		FT_UInt glyph_index = FT_Get_Char_Index(face, char_code);
		FT_Error err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);  // now we can access face->glyph
		assert(!err && "can't load a glyph");

		FT_Glyph glyph = nullptr;
		err = FT_Get_Glyph(face->glyph, &glyph);
		assert(!err && "unable to extract a glyph object");

		result.push_back(glyph);
	}

	return result;
}

unsigned width(FT_BBox const & b)
{
	return b.xMax - b.xMin;
}

unsigned height(FT_BBox const & b)
{
	return b.yMax - b.yMin;
}

void unite(FT_BBox & result, FT_BBox const & bbox)
{
	result.xMin = min(result.xMin, bbox.xMin);
	result.xMax = max(result.xMax, bbox.xMax);
	result.yMin = min(result.yMin, bbox.yMin);
	result.yMax = max(result.yMax, bbox.yMax);
}

void adjust(FT_BBox & result, FT_Pos dx)
{
	result.xMin += dx;
	result.xMax += dx;
}
