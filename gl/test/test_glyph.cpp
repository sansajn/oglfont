// do okna vyrenderuje jeden glyph
#include <iostream>
#include <string>
#include <Magick++.h>
#include <ft2build.h>
#include FT_GLYPH_H
#include FT_FREETYPE_H
#include "gl/window.hpp"
#include "gl/program.hpp"
#include "gl/mesh.hpp"
#include "gl/texture.hpp"
#include "gl/shapes.hpp"

using std::cout;
using std::string;
using gl::mesh;
using gl::make_quad_xy;


class main_window : public ui::glut_event_window
{
public:
	using base = ui::glut_event_window;
	main_window();
	void display() override;
};

texture2d render_glyph(unsigned char_code);
string pixel_mode(FT_Pixel_Mode m);

main_window::main_window()
{}

void main_window::display()
{
	static shader::program prog{"text.glsl"};
	prog.use();
	prog.uniform_variable("s", 0);

	static texture2d tex = render_glyph('T');
	tex.bind(0);

	static mesh quad = make_quad_xy<mesh>();
	quad.render();

	base::display();
}

int main(int argc, char * argv[])
{
	main_window w;
	w.start();
	return 0;
}

texture2d render_glyph(unsigned char_code)
{
	FT_Library ftlib;
	FT_Error err = FT_Init_FreeType(&ftlib);
	assert(!err && "unable to init a free-type library");

	FT_Face face;
	err = FT_New_Face(ftlib,
		"/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-B.ttf", 0, &face);
	assert(!err && "unable to load font face");

	unsigned const dpi = 96;
	unsigned font_size = 16 << 6;  // 26.6
	err = FT_Set_Char_Size(face, font_size, font_size, dpi, dpi);
	assert(!err && "freetype set size failed");

	FT_UInt glyph_index = FT_Get_Char_Index(face, char_code);
	err = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);  // now we can access face->glyph
	assert(!err && "can't load a glyph");

	FT_Glyph glyph = nullptr;
	err = FT_Get_Glyph(face->glyph, &glyph);
	assert(!err && "unable to extract a glyph object");

	FT_BitmapGlyph glyph_bitmap = (FT_BitmapGlyph)glyph;
	err = FT_Glyph_To_Bitmap((FT_Glyph *)&glyph_bitmap, FT_RENDER_MODE_NORMAL, nullptr, 0);
	assert(!err && "unable convert a glyph object to a glyph bitmap");

	FT_Bitmap & bitmap = glyph_bitmap->bitmap;

	cout
		<< "w:" << bitmap.width
		<< ", h:" << bitmap.rows
		<< ", pitch:" << bitmap.pitch
		<< ", num_grays:" << bitmap.num_grays
		<< ", pixel_mode:" << pixel_mode((FT_Pixel_Mode)bitmap.pixel_mode)
		<< ", size:" << (bitmap.width*bitmap.rows)
		<< std::endl;

	texture2d glyph_tex{bitmap.width, bitmap.rows, sized_internal_format::r8, pixel_format::red,
		pixel_type::ub8, bitmap.buffer, texture::parameters().mag(texture_filter::nearest)};

/*
	// [magick test]
	using namespace Magick;
	Image im{Geometry{bitmap.width, bitmap.rows}, "black"};
	im.depth(8);

	PixelPacket * pixels = im.getPixels(0, 0, bitmap.width, bitmap.rows);
	uint8_t const * buf = (uint8_t const *)bitmap.buffer;
	for (int i = 0; i < bitmap.width*bitmap.rows; ++i, ++buf, ++pixels)
		*pixels = ColorGray{*buf/255.0};
	im.syncPixels();

	Blob blob;
	im.write(&blob, "GRAY");

	texture2d glyph_tex(im.columns(), im.rows(), sized_internal_format::r8, pixel_format::red,
		pixel_type::ub8, blob.data(), texture::parameters().mag(texture_filter::nearest));
*/

	FT_Done_Glyph(glyph);
	FT_Done_Face(face);
	FT_Done_FreeType(ftlib);

	return glyph_tex;
}

string pixel_mode(FT_Pixel_Mode m)
{
	switch (m)
	{
		case FT_PIXEL_MODE_NONE: return "none";
		case FT_PIXEL_MODE_MONO: return "mono";
		case FT_PIXEL_MODE_GRAY: return "gray";
		case FT_PIXEL_MODE_GRAY2: return "gray2";
		case FT_PIXEL_MODE_GRAY4: return "gray4";
		case FT_PIXEL_MODE_LCD: return "lcd";
		case FT_PIXEL_MODE_LCD_V: return "lcd_v";
		case FT_PIXEL_MODE_BGRA: return "bgra";
		case FT_PIXEL_MODE_MAX: return "max";
		default: assert(0);
	}
}
