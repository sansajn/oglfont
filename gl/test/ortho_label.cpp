// test pouzitia ortho_label
#include <string>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "gl/program.hpp"
#include "gl/texture.hpp"
#include "gl/mesh.hpp"
#include "gl/window.hpp"
#include "gl/shapes.hpp"
#include "gl/controllers.hpp"
#include "gl/scene_object.hpp"
#include "label.hpp"

using std::string;
using std::to_string;
using glm::mat4;
using glm::vec3;
using glm::mat3;
using glm::vec2;
using glm::translate;
using glm::rotate;
using glm::scale;
using glm::inverseTranspose;
using gl::mesh;
using gl::shape_generator;
using shader::program;

char const * font_path = "/usr/share/fonts/truetype/ubuntu-font-family/UbuntuMono-R.ttf";

char const * shaded_shader_source = R"(
	// zozbrazi model s tienovanim zalozenom na normale vrchola
	uniform mat4 local_to_screen;
	uniform mat3 normal_to_world;
	uniform vec3 color = vec3(0.7, 0.7, 0.7);
	uniform vec3 light_dir = normalize(vec3(1,2,3));

	#ifdef _VERTEX_
	layout(location=0) in vec3 position;
	layout(location=2) in vec3 normal;
	out vec3 n;
	void main() {
		n = normal_to_world * normal;
		gl_Position = local_to_screen * vec4(position, 1);
	}
	#endif
	#ifdef _FRAGMENT_
	in vec3 n;
	out vec4 fcolor;
	void main() {
		fcolor = vec4(max(dot(n, light_dir), 0.2) * color, 1);
	}
	#endif
)";


class scene_window : public ui::glut_pool_window
{
public:
	using base = ui::glut_pool_window;
	scene_window();
	void update(float dt) override;
	void display() override;
	void input(float dt) override;
	void reshape(int w, int h) override;

private:
	mesh _cube, _box, _sphere;
	mesh _quad;
	ui::ortho_label _lbl;
	gl::free_camera<scene_window> _view;
	axis_object _axis;
	shader::program _shaded;
};

scene_window::scene_window()
	: _view{glm::radians(70.0f), aspect_ratio(), 0.01, 1000, *this}
{
	shape_generator<mesh> shape;
	_cube = shape.cube();
	_box = shape.box(vec3{.5, 1, .5});
	_sphere = shape.sphere(.5);
	_quad = shape.quad_xy();
	_lbl.init(font_path, 12, width(), height(), glm::vec2{2,2});
	_view.get_camera().position = vec3{3,3,3};
	_view.get_camera().look_at(vec3{0,0,0});
	_shaded.from_memory(shaded_shader_source);
	glClearColor(0,0,0,1);
}

void scene_window::update(float dt)
{
	base::update(dt);

	// update fps TODO: timer
	static float t = 0.0f;
	t += dt;
	if (t > 1.0f)
	{
		_lbl.text(string("fps: ") + to_string(fps()));
		t = t - 1.0f;
	}
}

void scene_window::display()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	mat4 world_to_screen = _view.get_camera().world_to_screen();

	// cube
	mat4 M{1};
	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen * M);
	_shaded.uniform_variable("normal_to_world", mat3{inverseTranspose(M)});
	_shaded.uniform_variable("color", rgb::lime);
	_cube.render();

	// box
	M = translate(vec3{2, 0, -2});
	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen * M);
	_shaded.uniform_variable("normal_to_world", mat3{inverseTranspose(M)});
	_shaded.uniform_variable("color", rgb::purple);
	_box.render();

	// sphere
	M = translate(vec3{2, 0, 1});
	_shaded.use();
	_shaded.uniform_variable("local_to_screen", world_to_screen * M);
	_shaded.uniform_variable("normal_to_world", mat3{inverseTranspose(M)});
	_shaded.uniform_variable("color", rgb::cyan);
	_sphere.render();

	_lbl.render();

	_axis.render(world_to_screen);
	base::display();
}

void scene_window::input(float dt)
{
	_view.input(dt);
	base::input(dt);
}

void scene_window::reshape(int w, int h)
{
	assert(w > 0 && h > 0 && "invalid screen geometry");
	_lbl.screen_geometry(w, h);
	base::reshape(w, h);
}


int main(int argc, char * argv[])
{
	scene_window w;
	w.start();
	return 0;
}
