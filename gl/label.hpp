#pragma once
#include <string>
#include <vector>
#include <map>
#include <glm/matrix.hpp>
#include <ft2build.h>
#include FT_GLYPH_H
#include FT_FREETYPE_H
#include "gl/program.hpp"
#include "gl/texture.hpp"

namespace ui {

/*! Renderovanie text-u.

Trieda ortho_label adresuje text takymto sposobom

	(0,0)
		+-----+
		|     |
		+-----+
			(800,600)

\code
ortho_label lbl;
lbl.init(font_path, 16, vec2{0,0});
// ...
lbl.render();
\endcode */
class ortho_label
{
public:
	ortho_label();
	~ortho_label();
	void init(std::string const & file_name, unsigned size, unsigned screen_w, unsigned screen_h, glm::vec2 const & pos = glm::vec2{0,0});
	void font(std::string const & file_name, unsigned size);
	void text(std::string const & s);
	void render();
	void position(glm::vec2 const & pos);
	void screen_geometry(unsigned w, unsigned h);
	unsigned width() const;
	unsigned height() const;

private:
	void build_text_texture();
	void build_transform_matrix();
	std::vector<FT_Glyph> load_glyphs(std::string const & s);

	std::string _text;
	glm::vec2 _pos;
	texture2d _text_tex;
	glm::mat4 _local_to_screen;
	shader::program _text_prog;
	unsigned _screen_w, _screen_h;
	FT_Library _lib;
	FT_Face _face;
	std::map<unsigned, FT_Glyph> _cache;
	unsigned const dpi = 96;
};

}  // ui
