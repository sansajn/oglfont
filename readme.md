# oglfont

## kompilácia

Ukážky a knižnicu skompilujeme príkazom

	~/gles2$ scons -j8


## štruktúra

	demos/
	gl/
	   libs/
	   test/
	   SConstruct
	   label.hpp
	   label.cpp
	gles2/

kde adresár gl/ je implementácia label-u pre OpenGL 3 a gles2/ je implementácia pre OpenGLES2 a adresár demos/ obsahuje ukážky tretích strán. Modul label (súbory label.hpp|.cpp) implementuje triedu label umožnujúcu render textu do OpenGL okna, pozri ukážku test/ortho_label.


## ukážky

**ortho_label.cpp**: ukážka použitia triedy ui::ortho_label v OpenGL
okne.

	test/test_glyph
	test/test_glyph_bmp_info
	test/test_magickload

